# Notre projet GLPI

## POUR : 

Une entreprise composée de cinquante utilisateurs.

## QUI VEULENT : 

un outil de gestion centralisé de suivi des incidents qu'ils rencontrent dans leur travail (GLPI).

## PRODUIT : 

    - un outil de gestion d'incidents centralisé GLPI dont les utilisateurs proviennent d'un Active Directory
    
## EST UN : 
    Composé de deux outils : 
    - Active Directory: L'objectif principal d'Active Directory est de fournir des services centralisés d'identification 
      et d'authentification à un réseau d'ordinateurs utilisant le système Windows, MacOs ou encore Linux.
    - GLPI : Outil de ticketing qui est un produit open source qui doit s'installer sur un serveur Linux.

## QUI : 

    - Permet aux utilisateurs de l'entreprise de créer des tickets lorsqu'ils ont des problèmes.
    - Permet à l'équipe IT de s'authentifier sur GLPI directement grâce à l'Active Directory.
    - Permet au support IT de créer et solutionner les demandes des utilisateurs.
## À LA DIFFÉRENCE DE :** FreshDesk, KPulse, ...

## NOTRE PRODUIT :

GLPI qui est un outil de ticketing gratuit synchronisé avec un Active Directory.

# Risques

## externes (clients, utilisateurs…)

    - Piratage ou phishing : fréquence aléatoire - impact très grave

## gestion de projet
    - Non respect des délais : fréquence faible - impact grave
    - Non respect du budget : fréquence faible - impact grave

## Risques Organisationnels

Mauvaise organisation - Productivité a la baisse : fréquence faible - impact grave

## Risques Techniques

Serveur HS : fréquence faible - impact très grave


Risque de sécurité : Risque d'attaque en cas de non sécurisation du serveur : fréquence moyenne  - impact fort

## Test création branche3

